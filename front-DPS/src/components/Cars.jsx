import makeStyles from "@material-ui/core/styles/makeStyles"

import useStore from "../utils/store"

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  },
  label: {
    fontSize: 20,
    marginBottom: 20
  }
})

const Cars = () => {
  const classes = useStyles()
  const {
    store: { user, cars }
  } = useStore()
  return (
    <div className={classes.root}>
      <div className={classes.label}>Автомобили</div>
      {user.drive_number !== "" &&
        cars.map((car, i) => (
          <div className="item" key={"car" + i}>
            <div className="item">id: {i}</div>
            <div className="item">Цена: {+car.price / 10 ** 18}</div>
            <div className="item">Категория: {car.category}</div>
            <div className="item">Срок эксплуатации: {car.srok}</div>
          </div>
        ))}
    </div>
  )
}

export default Cars
