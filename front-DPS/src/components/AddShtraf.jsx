import TextField from "@material-ui/core/TextField"

import useCustomState from "../utils/customState"
import useContract from "../utils/contract"
import useStore from "../utils/store"
import FormDialog from "./FormDialog"

const AddShtraf = () => {
  const {
    store: { address }
  } = useStore()

  const { state, onChangeState } = useCustomState({
    drive_number: ""
  })
  const { contract } = useContract()

  const addShtraf = async () => {
    try {
      await contract.methods.shtraf_dps(state.drive_number).send({ from: address })
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <FormDialog
      buttonTitle="Создать"
      title="Оштрафовать водителя"
      handleButtonTitle="Оштрафовать"
      handleButtonCallback={addShtraf}
    >
      <TextField
        autoFocus
        margin="dense"
        id="license_id"
        label="Номер водительского удостоверения"
        type="text"
        fullWidth
        value={state.drive_number}
        onChange={e => onChangeState("drive_number", e.target.value)}
      />
    </FormDialog>
  )
}

export default AddShtraf
