import TextField from "@material-ui/core/TextField"

import useCustomState from "../utils/customState"
import useContract from "../utils/contract"
import useStore from "../utils/store"
import useValidator from "../utils/validator"
import FormDialog from "./FormDialog"

const AddCar = () => {
  const {
    store: { address }
  } = useStore()
  const { state, onChangeState } = useCustomState({
    price: 0,
    srok: "",
    category: ""
  })
  const { contract, web3 } = useContract()
  const { valid } = useValidator({
    srok: [
      {
        validator: value => value <= 100,
        error: "Дата эксплуатации не может быть больше 100 лет"
      },
      {
        validator: value => value >= 0,
        error: "Дата эксплуатации не может быть меньше 0"
      }
    ]
  })
  const addCar = async () => {
    try {
      await contract.methods
        .registr_car(state.category, web3.utils.toWei(state.price.toString()), state.srok)
        .send({ from: address })
    } catch (e) {
      console.log(e)
    }
  }
  return (
    <FormDialog
      buttonTitle="Добавить"
      title="Добавить автомобиль"
      handleButtonTitle="Добавить"
      handleButtonCallback={() => valid({ srok: state.srok }) && addCar()}
    >
      <TextField
        autoFocus
        margin="dense"
        id="car_category"
        label="Категория"
        type="text"
        fullWidth
        value={state.category}
        onChange={e => onChangeState("category", e.target.value)}
      />
      <TextField
        margin="dense"
        id="car_price"
        label="Цена"
        type="text"
        fullWidth
        value={state.price}
        onChange={e => onChangeState("price", e.target.value)}
      />
      <TextField
        margin="dense"
        id="car_srok"
        label="Срок эксплуатации"
        type="text"
        fullWidth
        value={state.srok}
        onChange={e => onChangeState("srok", e.target.value)}
      />
    </FormDialog>
  )
}

export default AddCar
