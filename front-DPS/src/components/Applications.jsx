import Button from "@material-ui/core/Button"
import makeStyles from "@material-ui/core/styles/makeStyles"

import { secondsToDate } from "../utils/secondsToDate"
import useContract from "../utils/contract"
import useStore from "../utils/store"

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  },
  label: {
    fontSize: 20,
    marginBottom: 20
  }
})
const Applications = ({ getProfilData }) => {
  const classes = useStyles()
  const { contract } = useContract()
  const {
    store: { address, inputs_drive, inputs_car },
    onChangeStore
  } = useStore()

  const confirmCar = async (id, status) => {
    try {
      onChangeStore("isLoading", true)
      await contract.methods.registr_car_dps(id, status).send({ from: address })
      getProfilData()
    } catch (error) {
      console.log(error)
    }
    onChangeStore("isLoading", false)
  }

  const confirmDrive = async (id, status) => {
    try {
      onChangeStore("isLoading", true)
      await contract.methods.drive_add_dps(id, status).send({ from: address })
      getProfilData()
    } catch (error) {
      console.log(error)
    }
    onChangeStore("isLoading", false)
  }
  return (
    <div className={classes.root}>
      <div className="messages">
        <div className="label">Водительские удостоверения на рассмотрении</div>
        {inputs_drive
          .filter(input_Drive => input_Drive.category !== "")
          .map((input_Drive, i) => (
            <>
              <div className="item">ФИО: {input_Drive.FIO}</div>
              <div className="item">категория: {input_Drive.category}</div>
              <div className="item">номер водительского удостоверения: {input_Drive.drive_number}</div>
              <div className="item">дата окончания: {secondsToDate(+input_Drive.srok)}</div>
              <div>
                <Button onClick={() => confirmDrive(i, true)}>принять</Button>
                &nbsp;
                <Button onClick={() => confirmDrive(i, false)}>отклонить</Button>
              </div>
            </>
          ))}
      </div>
      <div className="messages">
        <div className="label">Транспортные средства на рассмотрении</div>
        {inputs_car
          .filter(({ car }) => car.category !== "")
          .map(({ car, driver, license }, i) => (
            <>
              <div className="item">ФИО: {driver.FIO}</div>
              <div className="item">категория автомобиля: {car.category}</div>
              <div className="item">категория вадительского удостоверения: {license.category}</div>
              <div>
                <Button onClick={() => confirmCar(i, true)}>принять</Button>
                &nbsp;
                <Button onClick={() => confirmCar(i, false)}>отклонить</Button>
              </div>
            </>
          ))}
      </div>
    </div>
  )
}

export default Applications
