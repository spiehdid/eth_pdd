import Button from "@material-ui/core/Button"
import makeStyles from "@material-ui/core/styles/makeStyles"

import { secondsToDate } from "../utils/secondsToDate"
import useContract from "../utils/contract"
import useStore from "../utils/store"
import AddLicense from "./AddLicense"
import AddCar from "./AddCar"
import AddStrah from "./AddStrah"

const useStyles = makeStyles({
  root: {
    padding: 10,
    background: "#fff",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    marginRight: 10,
    flexGrow: 1,
    borderRadius: 10
  },
  avatar: {
    height: 80,
    width: 80,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: "#FF9DC0",
    borderRadius: 80,
    color: "#fff"
  },
  data: {
    marginTop: 30
  },
  dataItem: {
    display: "flex",
    alignItems: "center",
    marginTop: 5
  }
})

const ProfileBar = ({ getProfilData }) => {
  const classes = useStyles()
  const { web3, contract } = useContract()
  const {
    store: { user, address, license },
    onChangeStore
  } = useStore()

  const changeSatus = async () => {
    try {
      onChangeStore("isLoading", true)
      await contract.methods.avto().send({ from: address })
      await getProfilData()
    } catch (error) {
      console.log(error)
    }
    onChangeStore("isLoading", false)
  }

  const updateLicense = async () => {
    try {
      onChangeStore("isLoading", true)
      await contract.methods.update_srok().send({ from: address })
      getProfilData()
    } catch (error) {
      console.log(error)
    }
    onChangeStore("isLoading", false)
  }

  return (
    <div className={classes.root}>
      <div className={classes.avatar}>AS</div>
      <div className={classes.data}>
        {(user.role === "0" || user.role === "1") && (
          <>
            <div className={classes.dataItem}>ФИО: {user.FIO}</div>
            <div className={classes.dataItem}>
              Режим проверки заявок: &nbsp;<span onClick={changeSatus}>{!user.avto ? "автоматический" : "ручной"}</span>
            </div>
            <div className={classes.dataItem}>Колличество ДТП: {user.dtp}</div>
            <div className={classes.dataItem}>Начало стажа: {user.staj}</div>
            <div className={classes.dataItem}>Страховой взнос: {web3.utils.fromWei(user.strah_vznos, "ether")} eth</div>
            <div className={classes.dataItem}>Колличество штрафов: {user._shtraf_dest}</div>
          </>
        )}
        <div className={classes.dataItem}>Баланс: {user.balance} eth</div>
        <div className="label" style={{ marginTop: 20 }}>
          Водительское удостоверение
        </div>
        {user.drive_number === "" ? (
          <AddLicense />
        ) : (
          <>
            <div className={classes.dataItem}>Номер: {user.drive_number}</div>
            <div className={classes.dataItem}>Категория: {license.category}</div>
            <div className={classes.dataItem}>Срок: {secondsToDate(+license.srok)}</div>
            <Button color="primary" variant="contained" onClick={updateLicense} style={{ marginTop: 10 }}>
              Продлить
            </Button>

            <div className={classes.dataItem} style={{ marginTop: 10 }}>
              Колличество автомобилей: {user.len_car} &nbsp;
              <AddCar />
            </div>
            {user.strah_vznos === "0" && user.len_car !== "0" && <AddStrah />}
          </>
        )}
      </div>
    </div>
  )
}

export default ProfileBar
