import List from "@material-ui/core/List"
import Drawer from "@material-ui/core/Drawer"
import ListItem from "@material-ui/core/ListItem"
import ListItemText from "@material-ui/core/ListItemText"
import makeStyles from "@material-ui/core/styles/makeStyles"

import { secondsToDate } from "../utils/secondsToDate"
import useStore from "../utils/store"

const useStyles = makeStyles({
  list: {
    width: 300
  }
})

const Messages = () => {
  const classes = useStyles()
  const {
    store: { isShowMessages, messages },
    onChangeStore
  } = useStore()

  return (
    <Drawer anchor="left" open={isShowMessages} onClick={() => onChangeStore("isShowMessages", false)}>
      <List className={classes.list}>
        {messages.map(({ content, time }, index) => (
          <ListItem key={content + index}>
            <ListItemText primary={content} secondary={secondsToDate(time)} />
          </ListItem>
        ))}
      </List>
    </Drawer>
  )
}

export default Messages
