import { useState } from "react"
import Button from "@material-ui/core/Button"
import Dialog from "@material-ui/core/Dialog"
import DialogActions from "@material-ui/core/DialogActions"
import DialogContent from "@material-ui/core/DialogContent"
import DialogTitle from "@material-ui/core/DialogTitle"
import CircularProgress from "@material-ui/core/CircularProgress"

const FormDialog = ({ buttonTitle, title, handleButtonTitle, handleButtonCallback, children, isEnd = true }) => {
  const [open, setOpen] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const handleClose = async () => {
    try {
      setIsLoading(true)
      await handleButtonCallback()
    } catch (e) {
      alert(e)
    }
    isEnd && setOpen(false)
    setIsLoading(false)
  }

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={() => setOpen(true)}>
        {buttonTitle}
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth="xs" fullWidth>
        <DialogTitle id="form-dialog-title">{title}</DialogTitle>
        <DialogContent>{children}</DialogContent>
        <DialogActions>
          {isLoading && <CircularProgress size={24} />}
          <Button onClick={() => setOpen(false)} color="primary" disabled={isLoading}>
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary" disabled={isLoading}>
            {handleButtonTitle}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default FormDialog
