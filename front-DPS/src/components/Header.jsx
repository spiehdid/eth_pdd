import { useHistory } from "react-router-dom"
import AppBar from "@material-ui/core/AppBar"
import Button from "@material-ui/core/Button"
import MailIcon from "@material-ui/icons/Mail"
import Toolbar from "@material-ui/core/Toolbar"
import Typography from "@material-ui/core/Typography"
import IconButton from "@material-ui/core/IconButton"
import makeStyles from "@material-ui/core/styles/makeStyles"
import createStyles from "@material-ui/core/styles/createStyles"

import useStore, { initStore } from "../utils/store"

const useStyles = makeStyles(theme =>
  createStyles({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    }
  })
)

const Header = () => {
  const classes = useStyles()
  const history = useHistory()
  const { onChangeStore } = useStore()

  const logout = () => {
    onChangeStore("store", initStore)
    sessionStorage.removeItem("address")
    history.push("/login")
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={() => onChangeStore("isShowMessages", true)}
          >
            <MailIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            ДПС
          </Typography>
          <Button color="inherit" onClick={() => history.push("/help")}>
            Помощь
          </Button>
          <Button color="inherit" onClick={() => history.push("/developers")}>
            Разработчики
          </Button>
          <Button color="inherit" onClick={logout}>
            Выйти
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default Header
