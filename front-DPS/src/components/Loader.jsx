import Backdrop from "@material-ui/core/Backdrop"
import makeStyles from "@material-ui/core/styles/makeStyles"
import CircularProgress from "@material-ui/core/CircularProgress"
import useStore from "../utils/store"

const useStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff"
  }
}))

const Loader = ({ children }) => {
  const classes = useStyles()
  const {
    store: { isLoading }
  } = useStore()
  return (
    <div>
      {children}
      <Backdrop className={classes.backdrop} open={isLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  )
}

export default Loader
