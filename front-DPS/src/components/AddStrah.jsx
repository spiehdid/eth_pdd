import TextField from "@material-ui/core/TextField"

import useCustomState from "../utils/customState"
import { sliceObj } from "../utils/sliceObj"
import useContract from "../utils/contract"
import useStore from "../utils/store"
import FormDialog from "./FormDialog"

const AddStrah = () => {
  const {
    store: { address }
  } = useStore()
  const { state, onChangeState } = useCustomState({
    drive_number: "",
    price: 0,
    step: 1
  })

  const { contract, web3 } = useContract()
  const addStrah = async () => {
    try {
      await contract.methods.strahovka_vznos().send({ from: address, value: state.price * 10 ** 18 })
      onChangeState("restore")
    } catch (e) {
      console.log(e)
    }
  }

  const checkStrah = async () => {
    try {
      await contract.methods.strahovka(state.drive_number).send({ from: address })
      const user = sliceObj(await contract.methods.check_user().call({ from: address }))
      onChangeState("price", web3.utils.fromWei(user.strah_summ.toString(), "ether"))
      onChangeState("step", 2)
    } catch (e) {
      console.log(e)
    }
  }
  return (
    <FormDialog
      buttonTitle="Оформить страховку"
      title="Оформление страховки"
      handleButtonTitle={state.step === 1 ? "Расчитать стоимость" : "Оформить"}
      handleButtonCallback={state.step === 1 ? checkStrah : addStrah}
      isEnd={state.step === 2}
    >
      <TextField
        autoFocus
        margin="dense"
        id="car_id"
        label="Номер автомобиля"
        type="text"
        fullWidth
        value={state.drive_number}
        onChange={e => onChangeState("drive_number", e.target.value)}
      />
      <TextField
        margin="dense"
        id="strah_sum"
        label="Сумма страховки"
        type="text"
        fullWidth
        value={state.price}
        disabled
      />
    </FormDialog>
  )
}

export default AddStrah
