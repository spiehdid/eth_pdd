import TextField from "@material-ui/core/TextField"

import useCustomState from "../utils/customState"
import { sliceObj } from "../utils/sliceObj"
import useContract from "../utils/contract"
import useStore from "../utils/store"
import FormDialog from "./FormDialog"

const AddDtp = () => {
  const {
    store: { address }
  } = useStore()
  const { state, onChangeState } = useCustomState({
    drive_number: ""
  })
  const { contract, web3 } = useContract()
  const addDtp = async () => {
    try {
      await web3.eth.personal.unlockAccount(process.env.REACT_APP_BANK_ADDRESS, "123", 9999)
      await web3.eth.personal.unlockAccount(process.env.REACT_APP_COMPANY_ADDRESS, "123", 9999)
      await contract.methods.dtp(state.drive_number).send({ from: address })
      const driver = sliceObj(await contract.methods.check_drive(state.drive_number).call({ from: address }))
      const user = sliceObj(await contract.methods.check_user().call({ from: driver.adr }))
      const companyBalance = await web3.eth.getBalance(process.env.REACT_APP_COMPANY_ADDRESS)

      if (+companyBalance < user.strah_vznos * 10) {
        await contract.methods.dolg_bank(state.drive_number).send({
          from: process.env.REACT_APP_BANK_ADDRESS,
          value: user.strah_vznos * 10 - companyBalance + 1000000000000000000
        })
      }
      await contract.methods.dtp_pay(state.drive_number).send({
        from: process.env.REACT_APP_COMPANY_ADDRESS,
        value: user.strah_vznos * 10
      })
    } catch (e) {
      console.log(e)
    }
  }
  return (
    <FormDialog buttonTitle="Создать" title="Оформить ДТП" handleButtonTitle="Оформить" handleButtonCallback={addDtp}>
      <TextField
        autoFocus
        margin="dense"
        id="license_id"
        label="Номер водительского удостоверения"
        type="text"
        fullWidth
        value={state.drive_number}
        onChange={e => onChangeState("drive_number", e.target.value)}
      />
    </FormDialog>
  )
}

export default AddDtp
