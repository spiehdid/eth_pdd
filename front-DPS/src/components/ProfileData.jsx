import makeStyles from "@material-ui/core/styles/makeStyles"

import Applications from "./Applications"
import useStore from "../utils/store"
import Strafs from "./Strafs"
import Cars from "./Cars"
import DTPS from "./DTPS"

const useStyles = makeStyles({
  root: {
    padding: 48,
    background: "#fff",
    display: "flex",
    flexGrow: 3,
    borderRadius: 10,
    position: "relative",
    overflow: "hidden"
  }
})

const ProfileData = ({ getProfilData }) => {
  const classes = useStyles()
  const {
    store: { user }
  } = useStore()

  return (
    <div className={classes.root}>
      {(user.role === "0" || user.role === "1") && (
        <>
          <Cars />
          <Strafs getProfilData={getProfilData} />
          <DTPS />
        </>
      )}
      {user.role === "1" && <Applications getProfilData={getProfilData} />}
    </div>
  )
}

export default ProfileData
