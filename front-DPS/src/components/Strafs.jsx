import Button from "@material-ui/core/Button"
import makeStyles from "@material-ui/core/styles/makeStyles"

import { secondsToDate } from "../utils/secondsToDate"
import useStore from "../utils/store"
import useContract from "../utils/contract"
import AddShtraf from "./AddShtraf"

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  },
  label: {
    fontSize: 20,
    marginBottom: 20
  }
})
const Strafs = ({ getProfilData }) => {
  const classes = useStyles()
  const { contract } = useContract()
  const {
    store: { user, shtrafs, address },
    onChangeStore
  } = useStore()

  const payShtraf = async id => {
    try {
      onChangeStore("isLoading", true)
      await contract.methods.shtraf_driver(id).send({ from: address, value: 10 * 10 ** 18 })
      getProfilData()
    } catch (error) {
      console.log(error)
    }
    onChangeStore("isLoading", false)
  }
  return (
    <div className={classes.root}>
      <div className={classes.label}>Штрафы</div>
      {shtrafs.map((shtraf, i) => (
        <>
          <div className="item">оплачено: {!shtraf.sost ? "да" : "нет"}</div>
          <div className="item">дата: {secondsToDate(+shtraf.time)}</div>
          {shtraf.sost && <Button onClick={() => payShtraf(i)}>оплатить</Button>}
        </>
      ))}
      {user.role === "1" && <AddShtraf />}
    </div>
  )
}

export default Strafs
