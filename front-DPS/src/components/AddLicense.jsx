import TextField from "@material-ui/core/TextField"

import useCustomState from "../utils/customState"
import useContract from "../utils/contract"
import { dateToSeconds } from "../utils/dateToSeconds"
import useStore from "../utils/store"
import FormDialog from "./FormDialog"

const AddLicense = () => {
  const {
    store: { address }
  } = useStore()
  const { state, onChangeState } = useCustomState({
    id: "",
    srok: "",
    category: ""
  })
  const { contract } = useContract()
  const addLicense = async () => {
    try {
      await contract.methods.drive_add(state.id, dateToSeconds(state.srok), state.category).send({ from: address })
    } catch (e) {
      console.log(e)
    }
  }
  return (
    <FormDialog
      buttonTitle="Добавить"
      title="Добавить водительское удостоверение"
      handleButtonTitle="Добавить"
      handleButtonCallback={addLicense}
    >
      <TextField
        autoFocus
        margin="dense"
        id="license_id"
        label="Номер"
        type="text"
        fullWidth
        value={state.id}
        onChange={e => onChangeState("id", e.target.value)}
      />
      <TextField
        margin="dense"
        id="license_date"
        label="Срок действия"
        type="date"
        InputLabelProps={{
          shrink: true
        }}
        fullWidth
        value={state.srok}
        onChange={e => onChangeState("srok", e.target.value)}
      />
      <TextField
        margin="dense"
        id="license_category"
        label="Категория"
        type="text"
        fullWidth
        value={state.category}
        onChange={e => onChangeState("category", e.target.value)}
      />
    </FormDialog>
  )
}

export default AddLicense
