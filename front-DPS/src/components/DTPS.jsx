import makeStyles from "@material-ui/core/styles/makeStyles"

import { secondsToDate } from "../utils/secondsToDate"
import useStore from "../utils/store"
import useContract from "../utils/contract"
import AddDtp from "./AddDtp"

const useStyles = makeStyles({
  root: {
    flexGrow: 1
  },
  label: {
    fontSize: 20,
    marginBottom: 20
  }
})
const DTPS = () => {
  const classes = useStyles()
  const { web3 } = useContract()
  const {
    store: { user, dtps }
  } = useStore()

  return (
    <div className={classes.root}>
      <div className={classes.label}>История ДТП</div>
      {dtps.map(dtp => (
        <>
          <div className="item">сумам выплаты: {web3.utils.fromWei(dtp.summ.toString(), "ether")}</div>
          <div className="item">дата: {secondsToDate(+dtp.time)}</div>
        </>
      ))}
      {user.role === "1" && <AddDtp />}
    </div>
  )
}

export default DTPS
