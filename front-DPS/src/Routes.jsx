import { useEffect } from "react"
import { Switch, Route, Redirect, useHistory } from "react-router-dom"

import Messages from "./components/Messages"
import Developers from "./pages/Developers"
import Header from "./components/Header"
import AddStrah from "./components/AddStrah"
import useStore from "./utils/store"
import SignIn from "./pages/SignIn"
import SignUp from "./pages/SignUp"
import Profil from "./pages/Profil"
import Help from "./pages/Help"

const Routes = () => {
  const history = useHistory()
  const {
    store: { address },
    onChangeStore
  } = useStore()

  useEffect(() => {
    const address = sessionStorage.getItem("address")
    if (!!address) {
      onChangeStore("address", address)
      history.push("/profil")
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      {address !== "" && <Header />}
      <Messages />
      <Switch>
        <Route path="/help" component={Help} />
        <Route path="/developers" component={Developers} />
        {address === "" ? (
          <Switch>
            <Route path="/signup" component={SignUp} />
            <Route path="/sign-in" component={SignIn} />
            <Redirect to="/sign-in" />
          </Switch>
        ) : (
          <Switch>
            <Route path="/profil" component={Profil} />
            <Redirect to="/profil" />
          </Switch>
        )}
        <Redirect to="/sign-in" />
      </Switch>
    </>
  )
}

export default Routes
