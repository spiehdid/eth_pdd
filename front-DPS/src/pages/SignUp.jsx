import Grid from "@material-ui/core/Grid"
import Paper from "@material-ui/core/Paper"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import { useHistory } from "react-router-dom"
import TextField from "@material-ui/core/TextField"
import Typography from "@material-ui/core/Typography"
import CssBaseline from "@material-ui/core/CssBaseline"
import makeStyles from "@material-ui/core/styles/makeStyles"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"

import useCustomState from "../utils/customState"
import useValidator from "../utils/validator"
import useContract from "../utils/contract"
import useStore from "../utils/store"

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor: theme.palette.type === "light" ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

const SignUp = () => {
  const classes = useStyles()
  const history = useHistory()
  const { onChangeStore } = useStore()
  const {
    state: { login, password, fio, staj, dtp, strafs, vznos, balance },
    onChangeState
  } = useCustomState({
    login: "",
    password: "",
    fio: "",
    staj: 0,
    dtp: 0,
    strafs: 0,
    vznos: 0,
    balance: 0
  })

  const { valid } = useValidator({
    staj: [
      {
        validator: value => value <= 100,
        error: "Стаж не может быть больше 100 лет"
      },
      {
        validator: value => value >= 0,
        error: "Стаж не может быть меньше 0"
      }
    ],
    dtp: [
      {
        validator: value => value <= 100,
        error: "Колличестыо ДТП не может быть больше 100"
      },
      {
        validator: value => value >= 0,
        error: "Колличестыо ДТП не может быть меньше 0"
      }
    ],
    strafs: [
      {
        validator: value => value <= 100,
        error: "Колличестыо штрафов не может быть больше 100"
      },
      {
        validator: value => value >= 0,
        error: "Колличестыо штрафов не может быть меньше 0"
      }
    ]
  })

  const { web3, contract } = useContract()

  const signUpHandler = async () => {
    onChangeStore("isLoading", true)
    const newUser = await web3.eth.personal.newAccount(password)
    const accounts = await web3.eth.getAccounts()
    await web3.eth.personal.unlockAccount(accounts[0], "123", 9999)
    await contract.methods
      .create_user(newUser, login, fio, staj, dtp, strafs, web3.utils.toWei(vznos.toString()))
      .send({ from: accounts[0] })
    await web3.eth.sendTransaction({
      from: accounts[0],
      to: newUser,
      value: balance * 10 ** 18
    })
    onChangeStore("isLoading", false)
    history.push("/login")
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign Up
          </Typography>
          <div className={classes.form}>
            <Grid container spacing={1}>
              <Grid item xs>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  label="Логин"
                  value={login}
                  onChange={e => onChangeState("login", e.target.value)}
                />
              </Grid>
              <Grid item xs>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  label="Пароль"
                  type="password"
                  value={password}
                  onChange={e => onChangeState("password", e.target.value)}
                />
              </Grid>
            </Grid>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label="ФИО"
              value={fio}
              onChange={e => onChangeState("fio", e.target.value)}
            />
            <Grid container spacing={1}>
              <Grid item xs>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  label="Стаж"
                  type="number"
                  max={100}
                  value={staj}
                  onChange={e => onChangeState("staj", e.target.value)}
                />
              </Grid>
              <Grid item xs>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  label="Колличество ДТП"
                  type="number"
                  value={dtp}
                  onChange={e => onChangeState("dtp", e.target.value)}
                />
              </Grid>
            </Grid>
            <Grid container spacing={1}>
              <Grid item xs>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  label="Колличество штрафов"
                  type="number"
                  value={strafs}
                  onChange={e => onChangeState("strafs", e.target.value)}
                />
              </Grid>
              <Grid item xs>
                <TextField
                  variant="outlined"
                  margin="normal"
                  fullWidth
                  label="Страховой взнос"
                  type="number"
                  value={vznos}
                  onChange={e => onChangeState("vznos", e.target.value)}
                />
              </Grid>
            </Grid>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label="Балансс"
              type="number"
              value={balance}
              onChange={e => onChangeState("balance", e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => valid({ staj, dtp, strafs }) && signUpHandler()}
            >
              Зарегестрироваться
            </Button>
          </div>
        </div>
      </Grid>
    </Grid>
  )
}

export default SignUp
