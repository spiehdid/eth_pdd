import { useEffect } from "react"
import makeStyles from "@material-ui/core/styles/makeStyles"

import ProfileData from "../components/ProfileData"
import ProfileBar from "../components/ProfileBar"
import { sliceObj } from "../utils/sliceObj"
import useContract from "../utils/contract"
import useStore from "../utils/store"

const useStyles = makeStyles(theme => ({
  root: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor: theme.palette.type === "light" ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
    display: "flex",
    padding: 30,
    minHeight: "calc(100vh - 64px)"
  }
}))

const Profil = () => {
  const classes = useStyles()
  const {
    store: { address },
    onChangeStore
  } = useStore()
  const { contract, web3 } = useContract()

  const getDataByLength = async (length, func, state) => {
    if (length) {
      const items = []
      for (let i = 0; i < length; i++) {
        const item = sliceObj(await contract.methods[func](i).call({ from: address }))
        items.push(item)
      }
      onChangeStore(state, items)
    }
  }

  const getProfilData = async () => {
    let existuser = {}
    if (address === process.env.REACT_APP_BANK_ADDRESS) {
      existuser = { role: "bank" }
    } else if (address === process.env.REACT_APP_COMPANY_ADDRESS) {
      existuser = { role: "company" }
    } else {
      existuser = sliceObj(await contract.methods.check_user().call({ from: address }))
    }
    if (!!existuser.drive_number && existuser.drive_number !== "") {
      const license = sliceObj(await contract.methods.check_drive(existuser.drive_number).call({ from: address }))
      onChangeStore("license", license)
    }
    const lenghts = sliceObj(await contract.methods.check_lens().call({ from: address }))
    const balance = (await web3.eth.getBalance(address)) / 10 ** 18
    const objUser = { ...existuser, ...lenghts, balance }
    onChangeStore("user", objUser)

    Promise.all([
      getDataByLength(+lenghts.len_car, "check_car", "cars"),
      getDataByLength(+lenghts.len_msg, "check_msg", "messages"),
      getDataByLength(+lenghts.len_shtrafs, "check_shtraf", "shtrafs"),
      getDataByLength(+lenghts.len_dtp, "check_dtp", "dtps")
    ])

    if (existuser.role === "1") {
      if (lenghts.len_input_drive !== "0") {
        let inputs_drive = []
        for (let i = 0; i < +lenghts.len_input_drive; i++) {
          const input_drive = sliceObj(await contract.methods.check_input_drive(i).call({ from: address }))
          const input_user = sliceObj(await contract.methods.check_user().call({ from: input_drive.adr }))
          inputs_drive.push({ ...input_user, ...input_drive })
        }
        onChangeStore("inputs_drive", inputs_drive)
      }
      if (lenghts.len_input_car !== "0") {
        let inputs_car = []
        for (let i = 0; i < +lenghts.len_input_car; i++) {
          const input_car = sliceObj(await contract.methods.check_input_car(i).call({ from: address }))
          const input_user = sliceObj(await contract.methods.check_user().call({ from: input_car.adr }))
          const license_user = sliceObj(
            await contract.methods.check_drive(input_user.drive_number).call({ from: address })
          )
          inputs_car.push({
            car: input_car,
            driver: input_user,
            license: license_user
          })
        }
        onChangeStore("inputs_car", inputs_car)
      }
    }


  }

  useEffect(() => {
    getProfilData()
    const timer = setInterval(getProfilData, 2000)

    return () => clearInterval(timer)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div className={classes.root}>
      <ProfileBar getProfilData={getProfilData} />
      <ProfileData getProfilData={getProfilData} />
    </div>
  )
}

export default Profil
