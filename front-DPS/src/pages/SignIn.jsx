import Grid from "@material-ui/core/Grid"
import Link from "@material-ui/core/Link"
import Paper from "@material-ui/core/Paper"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import { useHistory } from "react-router-dom"
import TextField from "@material-ui/core/TextField"
import Typography from "@material-ui/core/Typography"
import CssBaseline from "@material-ui/core/CssBaseline"
import makeStyles from "@material-ui/core/styles/makeStyles"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"

import useCustomState from "../utils/customState"
import useContract from "../utils/contract"
import useStore from "../utils/store"

const useStyles = makeStyles(theme => ({
  root: {
    height: "100vh"
  },
  image: {
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundRepeat: "no-repeat",
    backgroundColor: theme.palette.type === "light" ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

const SignIn = () => {
  const classes = useStyles()
  const history = useHistory()
  const {
    state: { login, password },
    onChangeState
  } = useCustomState({
    login: "",
    password: ""
  })
  const { onChangeStore } = useStore()
  const { web3, contract } = useContract()

  const loginHandler = async () => {
    onChangeStore("isLoading", true)
    const address = await contract.methods.return_adr(login).call()
    await web3.eth.personal.unlockAccount(address, password, 99999)
    sessionStorage.setItem("address", address)
    onChangeStore("address", address)
    onChangeStore("isLoading", false)
    history.push("/profil")
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <div className={classes.form}>
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label="Login"
              value={login}
              onChange={e => onChangeState("login", e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              fullWidth
              label="Password"
              type="password"
              value={password}
              onChange={e => onChangeState("password", e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={loginHandler}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item style={{ marginRight: 15 }}>
                <Link href="/help" variant="body2">
                  Помощь
                </Link>
              </Grid>
              <Grid item style={{ marginRight: 15 }}>
                <Link href="/developers" variant="body2">
                  Разработчики
                </Link>
              </Grid>
              <Grid item>
                <Link href="/signup" variant="body2">
                  Зарегестрироваться
                </Link>
              </Grid>
            </Grid>
          </div>
        </div>
      </Grid>
    </Grid>
  )
}

export default SignIn
