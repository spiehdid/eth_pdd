import { BrowserRouter as Router } from "react-router-dom"

import { ContractProvider } from "./utils/contract"
import { StoreProvider } from "./utils/store"
import Routes from "./Routes"
import Loader from "./components/Loader"

const App = () => (
  <StoreProvider>
    <Loader>
      <ContractProvider>
        <Router>
          <Routes />
        </Router>
      </ContractProvider>
    </Loader>
  </StoreProvider>
)

export default App
