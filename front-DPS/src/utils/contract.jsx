import Web3 from "web3"
import { createContext, useContext } from "react"

import abi from "../abi"

const ContractContext = createContext()

const useContract = () => useContext(ContractContext)

const ContractProvider = ({ children }) => {
  const web3 = new Web3("http://localhost:8545")
  const contract = new web3.eth.Contract(abi, "0x171Aa828F8B47833AAc59A4ab71253B32fD0901c")
  return <ContractContext.Provider value={{ web3, contract }}>{children}</ContractContext.Provider>
}

export { ContractProvider }
export default useContract
