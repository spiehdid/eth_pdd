import { useState } from "react"

const useCustomState = initState => {
  const [state, setState] = useState(initState)

  const onChangeState = (name = "restore", value = {}) => {
    setState(name === "restore" ? initState : { ...state, [name]: value })
  }

  return {
    state,
    onChangeState
  }
}

export default useCustomState
